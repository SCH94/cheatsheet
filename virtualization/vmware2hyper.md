Install Microsoft Virtual machine convertor

Powershell.exe -Command "& {Start-Process Powershell.exe -Verb RunAs}

Import-Module "C:\Program Files\Microsoft Virtual Machine Converter\MvmcCmdlet.psd1"

ConvertTo-MvmcVirtualHardDisk -SourceLiteralPath 'D:\iso\Kali 2022 x64 Customized by zSecurity 1.0.12.vmwarevm\Kali 2022 x64 Customized by zSecurity 1.0.12.vmwarevm\Disco virtual.vmdk' -VhdType DynamicHardDisk -VhdFormat Vhdx -DestinationLiteralPath 'D:\iso\kali\'


If you get error:

ConvertTo-MvmcVirtualHardDisk : The entry xxxx is not a supported disk database entry for the descriptor...

Download and extract dsfok tool

cd to dsfok

dsfo.exe "D:\PathToVMDK\disk1.vmdk" 512 1024 "D:\PathToVMDK\descriptor.txt"

Open descriptor.txt with Notepad++ or other text editor.

Find the line that appeared in the error above and comment it out by adding # in front of the line. Save the file.
The error above will only list the first offending entry, so you may need to repeat the process few times until you find and remove all problematic entries.

dsfi.exe "D:\PathToVMDK\disk1.vmdk" 512 1024 "D:\PathToVMDK\descriptor.txt"

Try running ConvertTo-MvmcVirtualHardDisk again.

Now import the disk to hyper-v

