# How to re-install Grub on Arch Linux
Is your [Arch Linux](https://www.addictivetips.com/ubuntu-linux-tips/how-to-install-arch-linux/) system’s [Grub bootloader broken](https://www.addictivetips.com/ubuntu-linux-tips/ubuntu-grub-repair-guide/)? Need to re-install it to make your Arch system bootable again? As it turns out, with a quick trick in the Arch Linux installer ISO, you can! In this guide, we’ll show you how!

![](data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAAUACgMBEQACEQEDEQH/xAGiAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgsQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+gEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoLEQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP4TvFfxO8Q6/c3BaWWzkF1cZkhvr64LRSfaElgP22a4byZTc3DMpc8Tywg+Q5joA8wJySeOTngAD8AMAD2AwO1AH//Z)

Before we begin
---------------

Fixing Grub in Arch Linux means connecting to the internet over the command-line in the installer. If you use Ethernet, the connection is automatic, and there’s nothing to configure. However, if you use WiFi, you’ll need to connect manually.

To connect to WiFi in the Arch Linux installer, [check out the official Wiki](https://wiki.archlinux.org/index.php/Network_configuration/Wireless). It will walk you through setting up a WiFi connection in the installer. 

Downloading Arch Linux ISO and creating USB
-------------------------------------------

![](https://www.addictivetips.com/app/uploads/2017/04/Arch-Linux-Boot-Menu.jpg)

If you need to reinstall Grub on Arch Linux, you’ll need the installation ISO on USB. If you’ve lost your original installation ISO file, you’ll need to download the ISO and create a new bootable USB.

Note: if you’ve still got your original Arch Linux USB installer, you may skip this section of the guide. 

To get your hands on the Arch Linux ISO file, head over to [ArchLinux.org.](http://archlinux.org/) Once on the website, find the “Download” button, and select it with the mouse. You’ll then download your Arch Linux ISO file to the spare PC or Mac you are making your Arch Linux USB on.

With the ISO file downloaded, head over to [Etcher.io](http://etcher.io/) and download the latest version of the app. Etcher is cross-platform and will run on Windows, Mac OS, and any Linux OS.

After downloading Etcher, open it up and follow the step-by-step instructions below to [create your new ISO file.](https://www.addictivetips.com/ubuntu-linux-tips/flash-multiple-usb-sticks-linux/) 

**Step 1:** Find the “Flash from file” button, and click on it with the mouse. When you select the “Flash from file” button, Etcher will open up a file browser window. Using this window, locate the Arch Linux ISO file and select it to add to the app.

**Step 2:** Insert your USB flash drive into the computer’s USB port. After inserting it, click on the “Select target” button and open up the device selection area. Then, use it to click on the USB flash drive you’ll use to install the Arch Linux ISO.

**Step 3:** Locate the “Flash!” button, and click on it with the mouse. Upon clicking on the “Flash!” button, Etcher will begin flashing the Arch Linux ISO file to your USB flash drive. 

When the flashing is complete, unplug the USB from the spare PC, and plug it into your Arch Linux system. Then, boot into the Arch PC’s [BIOS](https://www.addictivetips.com/windows-tips/access-bios-pc/), and set it up to boot from USB.

Mounting your Arch Linux installation 
--------------------------------------

You must mount your Arch Linux installation in the installer to re-install Grub. To do this, start by running the **lsblk** command. The **lsblk** command will list all attached storage devices on the system.

From here, locate your root partition. In this example, the root partition’s drive label is /dev/sda2. Please keep in mind that your root partition will differ. You will need to determine what your root partition is using **lsblk**.

Once you’ve figured out what your root partition’s drive label is, mount it to the `/mnt` folder using the **mount** command.

```
mount /dev/sda2 /mnt
```


After mounting the root partition, you will need to mount the boot partition. This part is tricky, and it differs depending on what type of installation your Arch Linux system is.

### UEFI

If you installed Arch Linux in UEFI mode, you likely have a dedicated UEFI partition of about 512 MB in size or so. In this example, the UEFI boot partition is **/dev/sda1** and mounts at `/boot/efi/` in Arch Linux.

Note: your UEFI boot partition label will differ from the 

To mount the UEFI partition in the installer, use the following **mount** command.

```
mount /dev/sda1 /mnt/boot/efi
```


Please keep in mind that each Arch Linux system is different. During the installation, you chose mount points for your UEFI boot partition. If your mount point differs from `/boot/efi`, you’ll need to specify this in place of the example command below to be able to access it in the Arch Linux installer.

Can’t figure out where your UEFI partition mounts to? Check out your fstab file with the command below. It outlines where each partition in Arch Linux mounts.

![](data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAAIACgMBEQACEQEDEQH/xAGiAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgsQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+gEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoLEQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP4u/iExPjfUrQkm1XxHqSrbEn7OFVDtUQ/6sKOwC4Haktl6L8gPku7AF3dAAAC4mAAGAAJGAAA4AA6CmB//2Q==)

```
cat /mnt/etc/fstab
```


### Non-UEFI

If you installed Arch Linux in Non-UEFI mode, the bootloader is installed directly to the root partition’s MBR. There’s no need to mount anything other than the root partition to fix grub.

```
pacman -S grub

grub-install --target=i386-pc /dev/sda

grub-mkconfig -o /boot/grub/grub.cfg
```

Re-installing grub
------------------

If your Grub is broken on Arch Linux, you can simply re-install it to fix it. To re-install it, you’ll need to first access your existing Arch Linux system in the Arch Chroot. To do that, enter the **arch-chroot** command below.

```
arch-chroot /mnt
```


By entering the command above, you’ll be accessing your Arch Linux installation. From here, Grub can easily be re-installed. 

#### Grub re-install UEFI

To re-install Grub on a UEFI system, enter the commands below. Remember to change **–efi-directory=/boot/efi** if your EFI directory isn’t **/boot/efi**.

![](data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAAUACgMBEQACEQEDEQH/xAGiAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgsQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+gEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoLEQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP4TvFfxO8Q6/c3BaWWzkF1cZkhvr64LRSfaElgP22a4byZTc3DMpc8Tywg+Q5joA8wJySeOTngAD8AMAD2AwO1AH//Z)

```
pacman -S grub efibootmgr os-prober

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub

grub-mkconfig -o /boot/grub/grub.cfg
```


#### Grub re-install Non-UEFI

![](data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAAUACgMBEQACEQEDEQH/xAGiAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgsQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+gEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoLEQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP4JfEvinWPE10Z9WnikkjaVAYIIrVGVnVsPFAqREKUUqAg2nJ6kmgDmaAP/2Q==)

To re-install Grub on a Non-UEFI system, enter the commands below. Be sure to change **/dev/sda** in the second command to reflect the hard drive you’ve installed Arch Linux on.

```
pacman -S grub

grub-install --recheck --target=i386-pc /dev/sda

grub-mkconfig -o /boot/grub/grub.cfg
```