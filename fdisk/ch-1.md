Partition The disk
---------------------


UEFI with GPT:
| Mount point on the installed system | Partition                 | Partition type        | Suggested size                               |
|-------------------------------------|---------------------------|-----------------------|----------------------------------------------|
| /boot1                              | /dev/efi_system_partition | EFI system partition  | 1 GiB                                        |
| [SWAP]                              | /dev/swap_partition       | Linux swap            | At least 4 GiB                               |
| /                                   | /dev/root_partition       | Linux x86-64 root (/) | Remainder of the device. At least 23–32 GiB. |


BIOS with MBR:
| Mount point on the installed system | Partition           | Partition type | Suggested size                               |
|-------------------------------------|---------------------|----------------|----------------------------------------------|
| [SWAP]                              | /dev/swap_partition | Linux swap     | At least 4 GiB                               |
| /                                   | /dev/root_partition | Linux          | Remainder of the device. At least 23–32 GiB. |


Fdisk Command in Linux (Create Disk Partitions)
===============================================

![Linux Fdisk Command](https://i3.wp.com/linuxize.com/post/fdisk-command-in-linux/featured_huc9db77d400abad9a3c9959c3a5afbcc1_20542_768x0_resize_q75_lanczos.jpg)

The first thing you need to do after installing a new SSD or hard disk is to partition it. A drive needs to have at least one partition before you can format it and store files on it.

In Linux, there are several tools that you can use to create partitions, with `fdisk` being the most commonly used one.

In this article, we will talk about the `fdisk` command.

`fdisk` is a menu-driven command-line utility that allows you to create and manipulate partition tables on a hard disk.

Be aware that `fdisk` is a dangerous tool and should be used with extreme caution. Only root or users with [`sudo`](https://linuxize.com/post/sudo-command-in-linux/) privileges can manipulate the partition tables.

List Partitions
---------------

To list the partition table of a device, invoke the `fdisk` command with the `-l` option, followed by the device name. For example to list the `/dev/sda` partition table and partitions you would run:

    fdisk -l /dev/sda

When no device is given as an argument, `fdisk` will print partition tables of all devices listed in the `/proc/partitions` file:

    fdisk -l

    Disk /dev/nvme0n1: 232.91 GiB, 250059350016 bytes, 488397168 sectors
    Disk model: Samsung SSD 960 EVO 250GB               
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes
    I/O size (minimum/optimal): 512 bytes / 512 bytes
    Disklabel type: gpt
    Disk identifier: 6907D1B3-B3AB-7E43-AD20-0707A656A1B5
    
    Device            Start       End   Sectors   Size Type
    /dev/nvme0n1p1     2048   1050623   1048576   512M EFI System
    /dev/nvme0n1p2  1050624  34605055  33554432    16G Linux swap
    /dev/nvme0n1p3 34605056 488397134 453792079 216.4G Linux filesystem
    
    
    Disk /dev/sda: 465.78 GiB, 500107862016 bytes, 976773168 sectors
    Disk model: WDC WD5000AAKS-0
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes
    I/O size (minimum/optimal): 512 bytes / 512 bytes
    Disklabel type: dos
    Disk identifier: 0x0001cca3
    
    Device     Boot Start       End   Sectors   Size Id Type
    /dev/sda1        2048 976771071 976769024 465.8G 83 Linux
    

The output above shows the current partition tables of all devices that are attached to your system. Generally, SATA device names follow the pattern `/dev/sd[a-z]`, while NVMe device names have the following pattern `/dev/nvme[1-9]n[1-9]`.

Creating Partition Table
------------------------

To start partitioning the drive, run `fdisk` with the device name. In this example we’ll work on `/dev/sdb`:

    fdisk /dev/sdb

The command prompt will change, and the `fdisk` dialogue where you can type in commands will open:

    Welcome to fdisk (util-linux 2.34).
    Changes will remain in memory only, until you decide to write them.
    Be careful before using the write command.
    
    Command (m for help):
    

Changes you make to the partition table won’t take effect until you write them with the `w` command. You can exit the `fdisk` dialogue without saving the changes using the `q` command.

To get a list of all available commands enter `m`:

    m

![fdisk commands](https://i3.wp.com/linuxize.com/post/fdisk-command-in-linux/fdisk-commands_hu3469665baa3effbebd88035f5c0ce412_64622_768x0_resize_q75_lanczos.jpg)

If you are partitioning a new drive, before starting to create partitions first, you need to create a partition table. Skip this step if the device already has a partition table and you want to keep it.

`fdisk` supports several partitioning schemes. MBR and GPT are the two most popular partition scheme standards, that store the partitioning information on a drive in a different way. GPT is a newer standard allowing and has many advantages over MBR. The main points to consider when choosing what partitioning standard to use:

*   Use MBR to boot the disk in legacy BIOS mode.
*   Use GPT to boot the disk in UEFI mode.
*   The MBR standard supports creating a disk partition up to 2 TiB. If you have a disk of 2 TiB or larger, use GPT.
*   MBR has a limit of 4 primary partitions. If you need more partitions, one of the primary partitions can be set as an extended partition and hold additional logical partitions. With GPT, you can have up to 128 partitions. GPT doesn’t support extended or logical partitions.

In this example, we will use a GPT partition table.

Enter `g` to create a new empty GPT partition table:

    g

The output will look something like this:

    Created a new GPT disklabel (GUID: 4649EE36-3013-214E-961C-51A9187A7503).
    

The next step is to create the new partitions.

We will create two partitions. The first one with a size of 100 GiB and the second one will take the rest of the disk space.

Run the `n` command to create a new partition:

    n

You’ll be prompted to enter the partition number. Hit “Enter” to use the default value (`1`):

    Partition number (1-128, default 1):
    

Next, the command will ask you to specify the first sector. Generally it is always recommended to use the default values for the first value. Hit “Enter” to use the default value (`2048`):

    First sector (2048-500118158, default 2048):
    

On the next prompt, you’ll need to enter the last sector. You can use an absolute value for the last sector or relative value to the start sector, using the + symbol following by the partition size. The size can be specified in kibibytes (K), mebibytes (M), gibibytes (G), tebibytes (T), or pebibytes (P).

Enter `+100G` to set the partition size to 100 GiB:

    Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-500118158, default 500118158): +100G
    

    Created a new partition 1 of type 'Linux filesystem' and of size 100 GiB.
    

By default, the type of the new partition is set to “Linux filesystem”, which should be fine for most cases. If you want to change the type, press `l` to get a list of partition types and then press `t` to change the type.

Let’s create the second partition that will take the rest of the disk space:

    n

Use the default values for the partition number, first and last sectors. This will create a partition that will use all available space on the disk.

    Partition number (2-128, default 2): 
    First sector (209717248-625142414, default 209717248): 
    Last sector, +/-sectors or +/-size{K,M,G,T,P} (209717248-625142414, default 625142414): 
    

Once done creating partitions, use the `p` command to display the new partition table:

    p

    Disk /dev/sdb: 298.9 GiB, 320072933376 bytes, 625142448 sectors
    Disk model: nal USB 3.0     
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 4096 bytes
    I/O size (minimum/optimal): 4096 bytes / 4096 bytes
    Disklabel type: gpt
    Disk identifier: F8365250-AF58-F74E-B592-D56E3A5DEED1
    
    Device         Start       End   Sectors   Size Type
    /dev/sdb1       2048 209717247 209715200   100G Linux filesystem
    /dev/sdb2  209717248 625142414 415425167 198.1G Linux filesystem
    

If you want to delete a partition, use the `d` command.

Save the changes by running the `w` command:

    p

The command will write the table to the disk and exit the `fdisk` menu.

    The partition table has been altered.
    Calling ioctl() to re-read partition table.
    Syncing disks.
    

The kernel will read the device partition table without the need to reboot the system.

Activating the Partitions
-------------------------

Now that the partitions have been created, the next step is to format the partitions and mount them to the system’s directory tree.

We’ll format both partitions to ext4:

    sudo mkfs.ext4 -F /dev/sdb1

    mke2fs 1.45.5 (07-Jan-2020)
    Creating filesystem with 51928145 4k blocks and 12984320 inodes
    Filesystem UUID: 63a3457e-c3a1-43f4-a0e6-01a7dbe7dfed
    Superblock backups stored on blocks: 
    	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
    	4096000, 7962624, 11239424, 20480000, 23887872
    
    Allocating group tables: done                            
    Writing inode tables: done                            
    Creating journal (262144 blocks): done
    Writing superblocks and filesystem accounting information: done   
    

In this example, will mount the partitions to `/mnt/audio` and `/mnt/video` directories.

Create the mount points with [`mkdir`](https://linuxize.com/post/how-to-create-directories-in-linux-with-the-mkdir-command/) :

    sudo mkdir -p /mnt/audio /mnt/video 

[Mount](https://linuxize.com/post/how-to-mount-and-unmount-file-systems-in-linux/) the new partition:

    sudo mount /dev/sdb1 /mnt/audio

Partitions will stay mounted until you unmount it or [shutdown](https://linuxize.com/post/linux-shutdown-command/) the machine. To automatically mount a partition when your Linux system starts up, define the mount in the `/etc/fstab` file.

That’s it! You can now use the new partitions to store your files.

We'll format swap partition.

    sudo mkswap /dev/sda2

    swapon /dev/sda2

Conclusion
----------

`fdisk` is a command-line tool for creating partition schemes. For more information about the `fdisk` command, type `man fdisk` in your terminal.

If you have any questions or feedback, feel free to leave a comment.
